# Spring Boot RESTful Application for AppDirect #
## 1.0 ##

This app is based on Spring Boot Web application. It uses ```spring-boot-starter-web``` and ```spring-boot-starter-data-jpa``` starter libs.

## Requirements ##

* Java 8
* Maven

## How to run? ##

* Run API - ```mvn spring-boot:run```
* Run Tests - ```mvn clean test```

## API Docs ##

* Create user - ```/api/users - POST```

Sample request data -

```
#!json

{
  "firstName": "Test",
  "lastName": "TestLast",
  "email": "test@last.com"
}
```


Sample response -

```
#!json

{
  "id": 1,
  "firstName": "Test",
  "lastName": "TestLast",
  "email": "test@last.com",
  "active": true
}
```


* Get active users - ```/api/users - GET```

Sample response -

```
#!json

[
  {
    "id": 1,
    "firstName": "Test",
    "lastName": "TestLast",
    "email": "test@last.com",
    "active": true
  },
  {
    "id": 2,
    "firstName": "Test 2",
    "lastName": "TestLast 2",
    "email": "test2@last.com",
    "active": true
  }
]
```


* Update user - ```/api/users - PUT```

Sample request data -

```
#!json

{
  "id": 1
  "firstName": "TestUpdated",
  "lastName": "TestLast",
  "email": "test@last.com"
}
```

Sample response -

```
#!json

{
  "id": 1,
  "firstName": "TestUpdated",
  "lastName": "TestLast",
  "email": "test@last.com",
  "active": true
}
```


* Get user - ```/api/users/<id> - GET```

Sample request - ```/api/users/1```

Sample response -

```
#!json

{
  "id": 1,
  "firstName": "Test",
  "lastName": "TestLast",
  "email": "test@last.com",
  "active": true
}
```


* Delete user - ```/api/users/<id> - DELETE```

Sample request - ```/api/users/1```

Sample response - ```<no response>```