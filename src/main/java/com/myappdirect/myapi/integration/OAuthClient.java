package com.myappdirect.myapi.integration;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * This can be used as a base for sending any OAuth signed requests to AppDirect API
 */
@Component
public class OAuthClient {

    @Value("${consumer.key}")
    private String consumerKey;

    @Value("${consumer.secret}")
    private String consumerSecret;

    public HttpResponse sendRequest(HttpUriRequest request) throws Exception {
        System.out.println("Consumer key: " + consumerKey);
        System.out.println("Consumer secret: " + consumerSecret);

        OAuthConsumer consumer = new CommonsHttpOAuthConsumer(consumerKey, consumerSecret);
        consumer.sign(request);

        System.out.println("Request: " + request.getURI().getPath());
        System.out.println("Authorization Header: " + request.getHeaders(HttpHeaders.AUTHORIZATION)[0].getValue());
        System.out.println("Sending request...");

        HttpClient httpClient = HttpClientBuilder.create().build();
        return httpClient.execute(request);
    }
}