package com.myappdirect.myapi.repository;

import com.myappdirect.myapi.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findByActive(Boolean active);
}
