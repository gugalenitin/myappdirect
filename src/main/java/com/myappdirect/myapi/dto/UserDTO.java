package com.myappdirect.myapi.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class UserDTO {
    private Long id;

    @NotEmpty
    private String firstName;

    private String lastName;

    @NotEmpty
    private String email;

    private Boolean active;
}
