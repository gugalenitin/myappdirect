package com.myappdirect.myapi.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class PaymentInstrumentDTO {
    @NotEmpty
    private String id;

    @NotEmpty
    private String paymentMethod;
}
