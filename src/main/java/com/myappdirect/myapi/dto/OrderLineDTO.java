package com.myappdirect.myapi.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class OrderLineDTO {
    private String id;

    @NotEmpty
    private String unit;

    @NotEmpty
    private String quantity;
}
