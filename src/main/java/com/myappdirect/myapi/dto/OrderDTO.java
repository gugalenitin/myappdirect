package com.myappdirect.myapi.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

@Getter
@Setter
public class OrderDTO {
    private String id;

    @NotEmpty
    private String paymentPlanId;

    private List<OrderLineDTO> orderLines;
}
