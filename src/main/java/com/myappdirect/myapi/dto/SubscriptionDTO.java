package com.myappdirect.myapi.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class SubscriptionDTO {
    private String id;

    @NotEmpty
    private OrderDTO order;
}
