package com.myappdirect.myapi.service;

import com.myappdirect.myapi.dto.CompanyDTO;
import com.myappdirect.myapi.dto.SubscriptionDTO;
import com.myappdirect.myapi.dto.UserDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface CompanyService {

    Optional<CompanyDTO> create(CompanyDTO company) throws Exception;

    List<CompanyDTO> list() throws Exception;

    Optional<CompanyDTO> findById(String id) throws Exception;

    void delete(String id) throws Exception;
}
