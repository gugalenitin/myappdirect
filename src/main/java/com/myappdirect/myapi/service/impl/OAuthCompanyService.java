package com.myappdirect.myapi.service.impl;

import com.myappdirect.myapi.dto.CompanyDTO;
import com.myappdirect.myapi.integration.OAuthClient;
import com.myappdirect.myapi.service.CompanyService;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class OAuthCompanyService implements CompanyService {

    private final OAuthClient client;

    private String apiEndpoint;

    @Autowired
    public OAuthCompanyService(OAuthClient client, @Value("${api.base}") String apiBase,
                               @Value("${api.account}") String apiAccount, @Value("${api.company}") String apiCompany) {
        this.client = client;
        this.apiEndpoint = createURL(apiBase, apiAccount, apiCompany);
    }

    @Override
    public Optional<CompanyDTO> create(CompanyDTO companyDTO) throws Exception {
        if (companyDTO == null || companyDTO.getId() != null) {
            return Optional.empty();
        }

        HttpPost post = new HttpPost(this.apiEndpoint);
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("name", companyDTO.getName()));
        post.setEntity(new UrlEncodedFormEntity(params));

        HttpResponse response = client.sendRequest(post);

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            return Optional.empty();
        }
        return getOptional(response.getEntity().getContent());
    }

    @Override
    public List<CompanyDTO> list() throws Exception {
        HttpGet get = new HttpGet(apiEndpoint);

        HttpResponse response = client.sendRequest(get);

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            return Collections.emptyList();
        }
        return getList(response.getEntity().getContent());
    }

    @Override
    public Optional<CompanyDTO> findById(String id) throws Exception {
        if (id == null) {
            return Optional.empty();
        }

        HttpGet get = new HttpGet(apiEndpoint + id);

        HttpResponse response = client.sendRequest(get);

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            return Optional.empty();
        }
        return getOptional(response.getEntity().getContent());
    }

    @Override
    public void delete(String id) throws Exception {
        if (id == null) {
            return;
        }

        HttpDelete delete = new HttpDelete(apiEndpoint + id);

        client.sendRequest(delete);
    }

    private String createURL(String apiBase, String apiAccount, String apiCompany) {
        apiAccount = new MessageFormat(apiAccount).format(new String[]{apiBase});
        return new MessageFormat(apiCompany).format(new String[]{apiAccount});
    }

    private CompanyDTO convertToDTO(JSONObject jsonObject) {
        CompanyDTO companyDTO = new CompanyDTO();
        companyDTO.setId(jsonObject.getString("id"));
        companyDTO.setName(jsonObject.getString("name"));
        return companyDTO;
    }

    private List<CompanyDTO> getList(InputStream response) {
        if (null == response) {
            return Collections.emptyList();
        }

        List<CompanyDTO> companies = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(response);
        for (int i = 0; i < jsonArray.length(); i++) {
            companies.add(convertToDTO(jsonArray.getJSONObject(i)));
        }
        return companies;
    }

    private Optional<CompanyDTO> getOptional(InputStream response) {
        Optional<CompanyDTO> companyDTO = Optional.empty();
        if (null != response) {
            JSONObject jsonObject = new JSONObject(response);
            companyDTO = Optional.of(convertToDTO(jsonObject));
        }
        return companyDTO;
    }

}
