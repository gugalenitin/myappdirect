package com.myappdirect.myapi.service.impl;

import com.myappdirect.myapi.dto.UserDTO;
import com.myappdirect.myapi.model.User;
import com.myappdirect.myapi.repository.UserRepository;
import com.myappdirect.myapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DBUserService implements UserService {

    private final UserRepository userRepository;

    public DBUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<UserDTO> create(UserDTO userDTO) {
        if (userDTO == null || userDTO.getId() != null) {
            return Optional.empty();
        }
        return Optional.of(activateAndSave(userDTO));
    }

    @Override
    public List<UserDTO> list() {
        return userRepository.findByActive(true)
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<UserDTO> findById(Long id) {
        return getOptional(userRepository.findOne(id));
    }

    @Override
    public Optional<UserDTO> update(UserDTO userDTO) {
        if (userDTO == null || userDTO.getId() == null) {
            return Optional.empty();
        }
        Optional<UserDTO> userDTOOpt = getOptional(userRepository.findOne(userDTO.getId()));
        if (userDTOOpt.isPresent()) {
            userDTOOpt = Optional.of(activateAndSave(userDTO));
        }
        return userDTOOpt;
    }

    @Override
    public void delete(Long id) {
        userRepository.delete(id);
    }

    @Override
    public void deactivate(Long id) {
        Optional<UserDTO> userDTO = getOptional(userRepository.findOne(id));
        if (userDTO.isPresent()) {
            deactivateAndSave(userDTO.get());
        }
    }

    private UserDTO activateAndSave(UserDTO userDTO) {
        userDTO.setActive(true);
        return convertToDTO(userRepository.save(convertToModel(userDTO)));
    }

    private UserDTO deactivateAndSave(UserDTO userDTO) {
        userDTO.setActive(false);
        return convertToDTO(userRepository.save(convertToModel(userDTO)));
    }

    private User convertToModel(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setActive(userDTO.getActive());
        return user;
    }

    private UserDTO convertToDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());
        userDTO.setActive(user.getActive());
        return userDTO;
    }

    private Optional<UserDTO> getOptional(User user) {
        Optional<UserDTO> userDTO = Optional.empty();
        if (null != user) {
            userDTO = Optional.of(convertToDTO(user));
        }
        return userDTO;
    }

}
