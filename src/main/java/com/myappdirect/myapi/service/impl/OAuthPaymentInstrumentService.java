package com.myappdirect.myapi.service.impl;

import com.myappdirect.myapi.dto.PaymentInstrumentDTO;
import com.myappdirect.myapi.integration.OAuthClient;
import com.myappdirect.myapi.service.PaymentInstrumentService;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class OAuthPaymentInstrumentService implements PaymentInstrumentService {

    private final OAuthClient client;

    private String apiEndpoint;

    @Autowired
    public OAuthPaymentInstrumentService(OAuthClient client, @Value("${api.base}") String apiBase,
                                         @Value("${api.billing}") String apiBilling, @Value("${api.paymentInstrument}") String apiPaymentInstrument) {
        this.client = client;
        this.apiEndpoint = createURL(apiBase, apiBilling, apiPaymentInstrument);
    }

    @Override
    public Optional<PaymentInstrumentDTO> create(PaymentInstrumentDTO paymentInstrumentDTO) throws Exception {
        if (paymentInstrumentDTO == null || paymentInstrumentDTO.getId() != null) {
            return Optional.empty();
        }

        HttpPost post = new HttpPost(this.apiEndpoint);
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("paymentMethod", paymentInstrumentDTO.getPaymentMethod()));
        post.setEntity(new UrlEncodedFormEntity(params));

        HttpResponse response = client.sendRequest(post);

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            return Optional.empty();
        }
        return getOptional(response.getEntity().getContent());
    }

    @Override
    public List<PaymentInstrumentDTO> list() throws Exception {
        HttpGet get = new HttpGet(apiEndpoint);

        HttpResponse response = client.sendRequest(get);

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            return Collections.emptyList();
        }
        return getList(response.getEntity().getContent());
    }

    @Override
    public Optional<PaymentInstrumentDTO> findById(String id) throws Exception {
        if (id == null) {
            return Optional.empty();
        }

        HttpGet get = new HttpGet(apiEndpoint + id);

        HttpResponse response = client.sendRequest(get);

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            return Optional.empty();
        }
        return getOptional(response.getEntity().getContent());
    }

    @Override
    public void delete(String id) throws Exception {
        if (id == null) {
            return;
        }

        HttpDelete delete = new HttpDelete(apiEndpoint + id);

        client.sendRequest(delete);
    }

    private String createURL(String apiBase, String apiBilling, String apiPaymentInstrument) {
        apiBilling = new MessageFormat(apiBilling).format(new String[]{apiBase});
        return new MessageFormat(apiPaymentInstrument).format(new String[]{apiBilling, "company_id", "user_id"});
    }

    private PaymentInstrumentDTO convertToDTO(JSONObject jsonObject) {
        PaymentInstrumentDTO paymentInstrumentDTO = new PaymentInstrumentDTO();
        paymentInstrumentDTO.setId(jsonObject.getString("id"));
        paymentInstrumentDTO.setPaymentMethod(jsonObject.getString("paymentMethod"));
        return paymentInstrumentDTO;
    }

    private List<PaymentInstrumentDTO> getList(InputStream response) {
        if (null == response) {
            return Collections.emptyList();
        }

        List<PaymentInstrumentDTO> companies = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(response);
        for (int i = 0; i < jsonArray.length(); i++) {
            companies.add(convertToDTO(jsonArray.getJSONObject(i)));
        }
        return companies;
    }

    private Optional<PaymentInstrumentDTO> getOptional(InputStream response) {
        Optional<PaymentInstrumentDTO> paymentInstrumentDTO = Optional.empty();
        if (null != response) {
            JSONObject jsonObject = new JSONObject(response);
            paymentInstrumentDTO = Optional.of(convertToDTO(jsonObject));
        }
        return paymentInstrumentDTO;
    }

}
