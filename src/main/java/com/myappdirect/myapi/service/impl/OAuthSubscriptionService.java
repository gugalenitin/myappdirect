package com.myappdirect.myapi.service.impl;

import com.myappdirect.myapi.dto.OrderDTO;
import com.myappdirect.myapi.dto.OrderLineDTO;
import com.myappdirect.myapi.dto.SubscriptionDTO;
import com.myappdirect.myapi.integration.OAuthClient;
import com.myappdirect.myapi.service.SubscriptionService;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class OAuthSubscriptionService implements SubscriptionService {

    private final OAuthClient client;

    private String apiEndpoint;

    private String cancelApiEndpoint;

    @Autowired
    public OAuthSubscriptionService(OAuthClient client, @Value("${api.base}") String apiBase,
                                    @Value("${api.billing}") String apiBilling,
                                    @Value("${api.subscription}") String apiSubscription,
                                    @Value("${api.subscription.cancel}") String cancelApiSubscription) {
        this.client = client;
        this.apiEndpoint = createURL(apiBase, apiBilling, apiSubscription);
        this.cancelApiEndpoint = createURL(apiBase, apiBilling, cancelApiSubscription);
    }

    @Override
    public Optional<SubscriptionDTO> create(SubscriptionDTO subscriptionDTO) throws Exception {
        if (subscriptionDTO == null || subscriptionDTO.getId() != null) {
            return Optional.empty();
        }

        HttpPost post = new HttpPost(this.apiEndpoint);
        JSONArray orderLines = new JSONArray();
        for (OrderLineDTO orderLineDTO : subscriptionDTO.getOrder().getOrderLines()) {
            JSONObject orderLine = new JSONObject();
            orderLine.put("unit", orderLineDTO.getUnit());
            orderLine.put("quantity", orderLineDTO.getQuantity());
            orderLines.put(orderLine);
        }
        JSONObject order = new JSONObject();
        order.put("paymentPlanId", subscriptionDTO.getOrder().getPaymentPlanId());
        order.put("orderLines", orderLines);
        JSONObject subscription = new JSONObject();
        subscription.put("order", order);
        post.setEntity(new StringEntity(subscription.toString()));

        HttpResponse response = client.sendRequest(post);

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            return Optional.empty();
        }
        return getOptional(response.getEntity().getContent());
    }

    @Override
    public List<SubscriptionDTO> list() throws Exception {
        HttpGet get = new HttpGet(apiEndpoint);

        HttpResponse response = client.sendRequest(get);

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            return Collections.emptyList();
        }
        return getList(response.getEntity().getContent());
    }

    @Override
    public Optional<SubscriptionDTO> findById(String id) throws Exception {
        if (id == null) {
            return Optional.empty();
        }

        HttpGet get = new HttpGet(apiEndpoint + id);

        HttpResponse response = client.sendRequest(get);

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            return Optional.empty();
        }
        return getOptional(response.getEntity().getContent());
    }

    @Override
    public void delete(String id) throws Exception {
        if (id == null) {
            return;
        }

        HttpDelete delete = new HttpDelete(cancelApiEndpoint + id);

        client.sendRequest(delete);
    }

    private String createURL(String apiBase, String apiBilling, String apiSubscription) {
        apiBilling = new MessageFormat(apiBilling).format(new String[]{apiBase});
        return new MessageFormat(apiSubscription).format(new String[]{apiBilling, "company_id", "user_id"});
    }

    private SubscriptionDTO convertToDTO(JSONObject jsonObject) {
        SubscriptionDTO subscriptionDTO = new SubscriptionDTO();
        subscriptionDTO.setId(jsonObject.getString("id"));
        subscriptionDTO.setOrder(convertToOrderDTO(jsonObject.getJSONObject("order")));
        return subscriptionDTO;
    }

    private OrderLineDTO convertToOrderLineDTO(JSONObject jsonObject) {
        OrderLineDTO orderLineDTO = new OrderLineDTO();
        orderLineDTO.setId(jsonObject.getString("id"));
        orderLineDTO.setUnit(jsonObject.getString("unit"));
        orderLineDTO.setQuantity(jsonObject.getString("quantity"));
        return orderLineDTO;
    }

    private OrderDTO convertToOrderDTO(JSONObject jsonObject) {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(jsonObject.getString("id"));
        orderDTO.setPaymentPlanId(jsonObject.getJSONObject("paymentPlan").getString("id"));

        List<OrderLineDTO> orderLineDTOs = new ArrayList<>();
        JSONArray orderLines = jsonObject.getJSONArray("orderLines");
        for (int i = 0; i < orderLines.length(); i++) {
            orderLineDTOs.add(convertToOrderLineDTO(orderLines.getJSONObject(i)));
        }
        orderDTO.setOrderLines(orderLineDTOs);
        return orderDTO;
    }

    private List<SubscriptionDTO> getList(InputStream response) {
        if (null == response) {
            return Collections.emptyList();
        }

        List<SubscriptionDTO> companies = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(response);
        for (int i = 0; i < jsonArray.length(); i++) {
            companies.add(convertToDTO(jsonArray.getJSONObject(i)));
        }
        return companies;
    }

    private Optional<SubscriptionDTO> getOptional(InputStream response) {
        Optional<SubscriptionDTO> subscriptionDTO = Optional.empty();
        if (null != response) {
            JSONObject jsonObject = new JSONObject(response);
            subscriptionDTO = Optional.of(convertToDTO(jsonObject));
        }
        return subscriptionDTO;
    }

}
