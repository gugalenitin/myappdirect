package com.myappdirect.myapi.service;

import com.myappdirect.myapi.dto.SubscriptionDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface SubscriptionService {

    Optional<SubscriptionDTO> create(SubscriptionDTO subscription) throws Exception;

    List<SubscriptionDTO> list() throws Exception;

    Optional<SubscriptionDTO> findById(String id) throws Exception;

    void delete(String id) throws Exception;
}
