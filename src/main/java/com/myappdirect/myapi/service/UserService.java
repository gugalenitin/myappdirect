package com.myappdirect.myapi.service;

import com.myappdirect.myapi.dto.UserDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface UserService {

    Optional<UserDTO> create(UserDTO user);

    List<UserDTO> list();

    Optional<UserDTO> findById(Long id);

    Optional<UserDTO> update(UserDTO user);

    void delete(Long id);

    void deactivate(Long id);
}
