package com.myappdirect.myapi.api;

import com.myappdirect.myapi.dto.UserDTO;
import com.myappdirect.myapi.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {
    // Delete request will only deactivate the user.
    private static final boolean SOFT_DELETE = true;
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public UserDTO register(@RequestBody UserDTO user) {
        return checkOptional(userService.create(user));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<UserDTO> list() {
        return userService.list();
    }

    @RequestMapping(value = "{userId}", method = RequestMethod.GET)
    public UserDTO get(@PathVariable Long userId) {
        return checkOptional(userService.findById(userId));
    }

    @RequestMapping(method = RequestMethod.PUT)
    public UserDTO update(@RequestBody UserDTO user) {
        return checkOptional(userService.update(user));
    }

    @RequestMapping(value = "{userId}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long userId) {
        if (SOFT_DELETE) {
            userService.deactivate(userId);
        } else {
            userService.delete(userId);
        }
    }

    private UserDTO checkOptional(Optional<UserDTO> userDTO) {
        if (userDTO.isPresent()) {
            return userDTO.get();
        }
        // Later can be wrapped in Response object with proper error handling
        return null;
    }
}
