package com.myappdirect.myapi.integration;

import org.apache.http.HttpResponse;

import static org.junit.Assert.assertEquals;

import org.apache.http.client.methods.HttpGet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OAuthClientTest {
    @Autowired
    private OAuthClient client;

    @Test
    public void sendRequest() throws Exception {
        HttpResponse response = client.sendRequest(new HttpGet("https://appdirect.com/api/account/v1/companies"));

        System.out.println("Response: " + response.getStatusLine());

        //assertEquals(200, response.getStatusLine().getStatusCode());
    }
}
