package com.myappdirect.myapi.utils;

import com.myappdirect.myapi.dto.UserDTO;
import com.myappdirect.myapi.model.User;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UserMock {
    public static UserDTO getUserDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName("TestFirst");
        userDTO.setLastName("TestLast");
        userDTO.setEmail("test@last.com");
        return userDTO;
    }

    public static User getUser() {
        User user = new User();
        user.setFirstName("TestFirst");
        user.setLastName("TestLast");
        user.setEmail("test@last.com");
        return user;
    }

    public static void assertUser(User expectedUser, User actualUser) {
        assertNotNull(actualUser);

        assertEquals(expectedUser.getFirstName(), actualUser.getFirstName());
        assertEquals(expectedUser.getLastName(), actualUser.getLastName());
        assertEquals(expectedUser.getEmail(), actualUser.getEmail());
    }

    public static void assertUserDTO(UserDTO expectedUser, UserDTO actualUser) {
        assertNotNull(actualUser);

        assertEquals(expectedUser.getFirstName(), actualUser.getFirstName());
        assertEquals(expectedUser.getLastName(), actualUser.getLastName());
        assertEquals(expectedUser.getEmail(), actualUser.getEmail());
    }

}
