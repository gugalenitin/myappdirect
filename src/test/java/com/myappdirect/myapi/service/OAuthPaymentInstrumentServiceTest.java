package com.myappdirect.myapi.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OAuthPaymentInstrumentServiceTest {
    @Autowired
    private PaymentInstrumentService paymentInstrumentService;

    @Test
    public void list() throws Exception {
        paymentInstrumentService.list();
    }

}