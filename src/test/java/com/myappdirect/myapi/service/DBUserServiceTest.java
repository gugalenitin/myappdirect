package com.myappdirect.myapi.service;

import com.myappdirect.myapi.dto.UserDTO;
import com.myappdirect.myapi.utils.UserMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DBUserServiceTest {
    @Autowired
    private UserService userService;

    @Test
    public void create() throws Exception {
        UserDTO userDTO = UserMock.getUserDTO();
        Optional<UserDTO> userDTOOpt = userService.create(userDTO);

        assertTrue(userDTOOpt.isPresent());
        UserMock.assertUserDTO(userDTO, userDTOOpt.get());
        assertTrue(userDTOOpt.get().getActive());
    }

    @Test
    public void list() throws Exception {
        UserDTO userDTO = UserMock.getUserDTO();
        userService.create(userDTO);
        userService.create(userDTO);
        userService.create(userDTO);

        List<UserDTO> userDTOs = userService.list();

        assertFalse(userDTOs.isEmpty());
    }

    @Test
    public void findById() throws Exception {
        UserDTO userDTO = UserMock.getUserDTO();
        Optional<UserDTO> userDTOOpt = userService.create(userDTO);
        userDTOOpt = userService.findById(userDTOOpt.get().getId());

        assertTrue(userDTOOpt.isPresent());
        UserMock.assertUserDTO(userDTO, userDTOOpt.get());
    }

    @Test
    public void update() throws Exception {
        UserDTO userDTO = UserMock.getUserDTO();
        Optional<UserDTO> userDTOOpt = userService.create(userDTO);
        userDTO = userDTOOpt.get();
        userDTO.setFirstName("TestFirstUpdated");
        userDTO.setActive(false);
        userDTOOpt = userService.update(userDTO);

        assertTrue(userDTOOpt.isPresent());
        assertEquals(userDTO.getFirstName(), userDTOOpt.get().getFirstName());
        assertTrue(userDTOOpt.get().getActive());
    }

    @Test
    public void delete() throws Exception {
        UserDTO userDTO = UserMock.getUserDTO();
        Optional<UserDTO> userDTOOpt = userService.create(userDTO);

        assertTrue(userDTOOpt.isPresent());

        userService.delete(userDTOOpt.get().getId());
        userDTOOpt = userService.findById(userDTOOpt.get().getId());

        assertFalse(userDTOOpt.isPresent());
    }

    @Test
    public void deactivate() throws Exception {
        UserDTO userDTO = UserMock.getUserDTO();
        Optional<UserDTO> userDTOOpt = userService.create(userDTO);

        assertTrue(userDTOOpt.get().getActive());

        userService.deactivate(userDTOOpt.get().getId());
        userDTOOpt = userService.findById(userDTOOpt.get().getId());

        assertFalse(userDTOOpt.get().getActive());
    }
}
