package com.myappdirect.myapi.repository;

import com.myappdirect.myapi.model.User;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.myappdirect.myapi.utils.UserMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void findByActive() throws Exception {
        User user = UserMock.getUser();
        user.setActive(true);

        this.entityManager.persist(user);
        List<User> users = userRepository.findByActive(true);
        assertFalse(users.isEmpty());

        User repoUser = users.get(0);

        UserMock.assertUser(user, repoUser);
        assertTrue(user.getActive());
    }

}
