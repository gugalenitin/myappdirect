package com.myappdirect.myapi.api;

import com.myappdirect.myapi.dto.UserDTO;
import com.myappdirect.myapi.service.UserService;
import com.myappdirect.myapi.utils.UserMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;

    @Test
    public void register() throws Exception {
        UserDTO userDTO = UserMock.getUserDTO();
        userDTO.setId(1l);
        userDTO.setActive(true);
        when(userService.create(any())).thenReturn(Optional.of(userDTO));
        this.mvc.perform(
                post("/api/users")
                        .content("{\"firstName\":\"TestFirst\",\"lastName\":\"TestLast\",\"email\":\"test@last.com\"}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"firstName\":\"TestFirst\",\"lastName\":\"TestLast\",\"email\":\"test@last.com\",\"active\":true}"));
    }

    @Test
    public void list() throws Exception {
        UserDTO userDTO = UserMock.getUserDTO();
        userDTO.setId(1l);
        userDTO.setActive(true);
        when(userService.list()).thenReturn(Collections.singletonList(userDTO));
        this.mvc.perform(
                get("/api/users")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"id\":1,\"firstName\":\"TestFirst\",\"lastName\":\"TestLast\",\"email\":\"test@last.com\",\"active\":true}]"));
    }

    @Test
    public void testGet() throws Exception {
        UserDTO userDTO = UserMock.getUserDTO();
        userDTO.setId(1l);
        userDTO.setActive(true);
        when(userService.findById(1l)).thenReturn(Optional.of(userDTO));
        this.mvc.perform(
                get("/api/users/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"firstName\":\"TestFirst\",\"lastName\":\"TestLast\",\"email\":\"test@last.com\",\"active\":true}"));
    }

    @Test
    public void update() throws Exception {
        UserDTO userDTO = UserMock.getUserDTO();
        userDTO.setFirstName("TestFirstUpdated");
        userDTO.setId(1l);
        userDTO.setActive(true);
        when(userService.update(any())).thenReturn(Optional.of(userDTO));
        this.mvc.perform(
                put("/api/users")
                        .content("{\"firstName\":\"TestFirstUpdated\",\"lastName\":\"TestLast\",\"email\":\"test@last.com\"}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"firstName\":\"TestFirstUpdated\",\"lastName\":\"TestLast\",\"email\":\"test@last.com\",\"active\":true}"));
    }

    @Test
    public void testDelete() throws Exception {
        this.mvc.perform(delete("/api/users/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }
}
